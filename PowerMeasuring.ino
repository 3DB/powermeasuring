#define PIN_READ_V    D3  // Read voltage
#define PIN_READ_A    D4  // Read current

#define READING_DELAY 2   // Millis

double cur_voltage = 0;   // Volts
double cur_current = 0;   // Ampere
double cur_power = 0;     // Watt
double used_energy = 0;   // Wh

long last_millis = 0;     // Time since last measurement

void setup() {
  Wire.begin(69);
  Wire.onReceive(on_i2c_packet);

  // Pins
  pinMode(PIN_READ_V, INPUT);
  pinMode(PIN_READ_A, INPUT);
}

void loop() {
  last_millis = millis();
  
  cur_voltage = (analogRead(PIN_READ_V) * (5.0 / 4095.0) - 2.5) * 20;
  cur_current = (analogRead(PIN_READ_A) * (5.0 / 4095.0) - 2.5) * 4;
  cur_power = cur_voltage * cur_current;
  used_energy += cur_power / (1000 / READING_DELAY) / 60 / 60;

  // Wait until next measuring
  while (millis() - last_millis < READING_DELAY) {}
}

void on_i2c_packet(int length) {
  if (Wire.read() == 0x42) {  // Magic byte
    switch(Wire.read()) {
      case 0x01:                  // Read voltage & current
        Wire.write(cur_voltage);
        Wire.write(cur_current);
      case 0x02:                  // Read power
        Wire.write(cur_power);
      case 0x03:                  // Read used energy
        Wire.write(used_energy);
        used_energy = 0;
      case 0x04:                  // Read current voltage
        Wire.write(cur_voltage);
      case 0x05:                  // Read current current
        Wire.write(cur_current);
    }
  }
}
